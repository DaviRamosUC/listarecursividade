package application;

public class Exec04 {

	public static void main(String[] args) {

		System.out.println(somarDigitos("2000"));
	}

	private static int somarDigitos(String string) {

		int resultado = 0;

		String valor = string.substring(0,1);
		resultado += Integer.parseInt(valor);
		string = string.substring(string.length() - (string.length() - 1));
		if (string.length() != 0) {
			resultado+=somarDigitos(string);
		}

		return resultado;
	}

}
