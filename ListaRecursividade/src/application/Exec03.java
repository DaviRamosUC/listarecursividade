package application;

public class Exec03 {

	public static void main(String[] args) {

		System.out.println(decimalBinario(5));
	}

	private static String decimalBinario(int n) {

		String resultado = "";
		if (n > 0) {
			resultado = "" + n % 2;
			n = n / 2;
			
			resultado = decimalBinario(n) + resultado;
		}

		return resultado;
	}

}
