package application;

public class Exec02 {

	public static void main(String[] args) {
		
		System.out.println(fatorial(7));
	}

	private static int fatorial(int i) {
		int r=1;
		if (i == 0) {
			return 1;
		}else if (i == 1) {
			return 1;
		}else {
			r *= i*fatorial(i-1);
		}
		
		return r;
	}

}
