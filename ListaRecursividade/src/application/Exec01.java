package application;

public class Exec01 {

	public static void main(String[] args) {
		System.out.println(CalculaMDC2(8,9));
	}

	public static int CalculaMDC2(int m, int n) {
		int r;
		r = m % n;
		m = n;
		n = r;
		if (r != 0) {
			m = CalculaMDC2(m, n);
		}
		return m;
	}

}
